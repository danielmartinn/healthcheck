from . import models
from django import forms

JENIS_KELAMIN = (
    ("Laki-laki", "Laki-laki"),
    ("Perempuan", "Perempuan"),
)

LAMA_OLAHRAGA = (
    ("< 1 jam", "< 1 jam"),
    ("1-2 jam", "1-2 jam"),
    ("2-3 jam", "2-3 jam"),
    ("> 3 jam", "> 3 jam"),
    ("Tidak sama sekali", "Tidak sama sekali")
)

AKTIVIAS_FISIK = (
    ("Sedikit", "Sedikit"),
    ("Sedang", "Sedang"),
    ("Banyak", "Banyak"),
)

KONDISI_STRESS = (
    ("Tidak Stress", "Tidak Stress"),
    ("Cukup Stress", "Cukup Stress"),
    ("Sangat Stress", "Sangat Stress"),
)


class FormHealthCheck(forms.ModelForm):
    usia = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "required": True,
        "placeholder": "ex : 20",
        "min": "0",
    }), label="Usia")

    jenis_kelamin = forms.ChoiceField(widget=forms.Select(
        attrs={"class": "form-control"}), choices=JENIS_KELAMIN, label="Jenis Kelamin")

    berat_badan = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "required": True,
        "placeholder": "ex : 56",
        "min": "0",
    }), label="Berat Badan (kg)")

    tinggi_badan = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "required": True,
        "placeholder": "ex : 170",
        "min": "0",
    }), label="Tinggi Badan (cm)")

    makan_buah = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "required": True,
        "placeholder": "ex : 4",
        "min": "0",
    }), label="Berapa kali makan buah dalam seminggu?")

    makan_sehari = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "required": True,
        "placeholder": "ex : 3",
        "min": "0",
    }), label="Berapa kali makan dalam sehari? (tidak termasuk makan ringan)")

    sayur = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "required": True,
        "placeholder": "ex : 1",
        "min": "0",
    }), label="Berapa kali makan sayur dalam sehari?")

    minum = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "required": True,
        "placeholder": "ex : 8",
        "min": "0",
    }), label="Berapa gelas yang diminum dalam satu hari? (1 gelas sekitar 200ml)")

    jumlah_bab = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "required": True,
        "placeholder": "ex : 7",
        "min": "0",
    }), label="Berapa kali buang air besar (BAB) dalam seminggu?")

    olahraga = forms.ChoiceField(widget=forms.Select(
        attrs={"class": "form-control"}), choices=LAMA_OLAHRAGA, label="Berapa lama olahraga dalam seminggu?")

    aktivitas_fisik = forms.ChoiceField(widget=forms.Select(
        attrs={"class": "form-control"}), choices=AKTIVIAS_FISIK, label="Bagaimana aktivitas fisik yang dilakukan sehari-hari? (selain olahraga)")

    tidur = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "required": True,
        "placeholder": "ex : 8",
        "min": "0",
        "max": "24",
    }), label="Berapa lama tidur dalam sehari? (jam)")

    stress = forms.ChoiceField(widget=forms.Select(
        attrs={"class": "form-control"}), choices=KONDISI_STRESS, label="Apakah kondisi stress anda belakangan ini?")

    class Meta:
        model = models.HealthCheckModel
        fields = ["usia", "jenis_kelamin", "berat_badan", "tinggi_badan", "makan_buah", "makan_sehari",
                  "sayur", "minum", "jumlah_bab", "olahraga", "aktivitas_fisik", "tidur", "stress"]
