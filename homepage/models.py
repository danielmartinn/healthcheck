from django.db import models
# Create your models here.


class HealthCheckModel(models.Model):
    usia = models.CharField(max_length=50)
    jenis_kelamin = models.CharField(max_length=50)
    berat_badan = models.CharField(max_length=50)
    tinggi_badan = models.CharField(max_length=50)
    makan_buah = models.CharField(max_length=50)
    makan_sehari = models.CharField(max_length=50)
    sayur = models.CharField(max_length=50)
    minum = models.CharField(max_length=50)
    jumlah_bab = models.CharField(max_length=50)
    olahraga = models.CharField(max_length=50)
    aktivitas_fisik = models.CharField(max_length=50)
    tidur = models.CharField(max_length=50)
    stress = models.CharField(max_length=50)

    def __str__(self):
        return self.usia
