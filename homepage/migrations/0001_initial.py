# Generated by Django 3.1.3 on 2020-11-21 02:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HealthCheckModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('usia', models.CharField(max_length=50)),
                ('jenis_kelamin', models.CharField(max_length=50)),
                ('berat_badan', models.CharField(max_length=50)),
                ('tinggi_badan', models.CharField(max_length=50)),
                ('makan_buah', models.CharField(max_length=50)),
                ('makan_sehari', models.CharField(max_length=50)),
                ('sayur', models.CharField(max_length=50)),
                ('minum', models.CharField(max_length=50)),
                ('jumlah_bab', models.CharField(max_length=50)),
                ('olahraga', models.CharField(max_length=50)),
                ('aktivitas_fisik', models.CharField(max_length=50)),
                ('tidur', models.CharField(max_length=50)),
                ('stress', models.CharField(max_length=50)),
                ('keadaan_tubuh', models.CharField(max_length=50)),
            ],
        ),
    ]
