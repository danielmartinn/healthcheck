from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.conf import settings
from .forms import FormHealthCheck
from .models import HealthCheckModel
from .decisiontree import *

import os
import json

# Create your views here.
node_dict = None
dt = None


def init_dt():
    global dt
    json_data = os.path.join(
        settings.BASE_DIR, 'homepage/static/healthcheckdata.json')

    node_dict = {}

    with open(json_data, "r") as json_file:
        node_dict = json.load(json_file)

    dt = dict_to_node(node_dict)


def index(request):
    form = FormHealthCheck()
    if request.method == "POST":
        form = FormHealthCheck(request.POST)
        if form.is_valid():
            request.session['input_data'] = form.cleaned_data
        return redirect('homepage:hasil')
    return render(request, 'index.html', {'form': form})


def hasil(request):
    if dt == None:
        init_dt()
    prediction, decisive_node = classify_bt(request.session['input_data'], dt, [])
    return render(request, 'hasil.html', {'prediction': prediction, 'decisive_node': decisive_node})
